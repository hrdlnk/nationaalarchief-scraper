(() => {

    // https://github.com/mholt/PapaParse/issues/175#issuecomment-514922286
    // Werkt in Chrome, Firefox en Safari op 12 aug 2020
    const fileSaver = (data, filename, mimetype) => {

        if (!data) return

        let blob = data.constructor !== Blob ?
            new Blob([data], {
                type: mimetype || 'application/octet-stream'
            }) :
            data

        if (navigator.msSaveBlob) {
            navigator.msSaveBlob(blob, filename)
            return
        }

        let lnk = document.createElement('a'),
            url = window.URL,
            objectURL

        if (mimetype) {
            lnk.type = mimetype
        }

        lnk.download = filename || 'untitled'
        lnk.href = objectURL = url.createObjectURL(blob)
        lnk.dispatchEvent(new MouseEvent('click'))
        setTimeout(url.revokeObjectURL.bind(url, objectURL))

    }

    // Formulier posts behandelen
    let formulier = document.querySelector('#formulier')

    const posten = q => fetch('/endpoint', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(q)
    })
        .catch((err) => {
            console.log(err)
        })

    formulier.addEventListener('submit', e => {
        e.preventDefault();
        // console.log('e:', e);
        [...document.querySelectorAll('.lader-container > *')].forEach(item => item.classList.toggle('onzichtbaar'))
        document.querySelector('.scraper > button').classList.toggle('bezig')
        let q = e.srcElement.querySelector('#input').value.split('\n');
        (async () => {
            let res = await posten(q)
            let data = await res.json()
            // console.log('data:', data)
            document.querySelector('.scraper > button').classList.toggle('bezig')
            let tijdStempel = data[1].timestamp
            document.querySelector('#csv-data').action = `data/Metadata-${tijdStempel}.csv`
            document.querySelector('#csv-data').addEventListener('click', e => {
                e.preventDefault()
                fileSaver(data[1].csv, `Metadata-${tijdStempel}.csv`, 'text/csv')
            })
            document.querySelector('#json-data').action = `data/Metadata-${tijdStempel}.json`
            document.querySelector('#json-data').addEventListener('click', e => {
                e.preventDefault()
                fileSaver(JSON.stringify(data[1].json), `Metadata-${tijdStempel}.json`, 'application/json')
            })
            document.querySelector('.loading').classList.add('onzichtbaar')
            document.querySelector('.json').innerHTML = JSON.stringify({
                timestamp: tijdStempel,
                request: data[0].body,
                csv: data[1].csv,
                json: data[1].json
            }, null, 2)
            document.querySelector('.json').classList.remove('onzichtbaar')
            document.querySelector('.data').classList.toggle('onzichtbaar')
        })()
    })
})()