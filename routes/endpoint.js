const express = require('express');
const router = express.Router();
const Scraper = require('../utils/scraper');

router.post('/', async (req, res, next) => {
  let data = await Scraper.scraper(req.body);
  res.send([{body: JSON.stringify(req.body)}, data])
});

module.exports = router;