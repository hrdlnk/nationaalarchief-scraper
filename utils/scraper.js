const fs = require('fs').promises
const fetch = require('node-fetch')
const HTMLParser = require('node-html-parser')
const ObjectsToCsv = require('objects-to-csv')

let headers = {
    // 'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:79.0) Gecko/20100101 Firefox/79.0'
    // 'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36'
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36'
}

const pauzeren = (min, max) => setTimeout(() => {
    const randomNummerRange = (min, max) => Math.random() * (max - min) + min
    let s = randomNummerRange(min, max) * 1000
    console.log(`\n${s} ms gepauzeerd.\n`), s
})

// let urls = ["http://proxy.handle.net/10648/6b004a04-1ad9-102f-a76c-003048944028", "http://proxy.handle.net/10648/aac798a6-d0b4-102d-bcf8-003048976d84", "http://proxy.handle.net/10648/6c98a276-1ad9-102f-a76c-003048944028"];

const metadataOphalen = async (n, url) => {
    let fotoMetadata = {}
    fotoMetadata['id'] = `${n}`.padStart(3, "0")
    try {
        let data = await fetch(`${url}`, headers)
        console.log('data.status:', await data.status);
        let getStatus = await data.status
        // foutbehandeling
        // in geval van geen metadata beschikbaar, bijv:
        // http://proxy.handle.net/10648/69f808fe-1ad9-102f-a76c-003048944028 (permanent)
        // https://www.nationaalarchief.nl/onderzoeken/fotocollectie/sfa%3Acol1%3Adat70 (canonical)
        if (getStatus == 200) {
            fotoMetadata['status'] = 'OK'
            let html = await data.text()
            let root = HTMLParser.parse(html)
            let container = root.querySelector('.field-data')
            let velden = container.querySelectorAll('.field')
            
            // Gegevens omzetten naar json
            velden.forEach(item => {
                // Uitsluitend alfanumerieke label (ivm key)
                let label = item.querySelector('.label').text.replace(/[^a-z0-9]/gi, '').toLowerCase()
                let value = item.querySelector('.value').text
                fotoMetadata[label] = `${value}`
            })
            return fotoMetadata
        } else {
            return { id: `${fotoMetadata.id}`, status: 404, url: `${url}` }
        }

    } catch (err) {
        console.log(err);
    }
}

exports.scraper = async urls => {
    try {
        let n = 1
        let fotosMetadata = []
        for (let url of [...urls]) {
            if (url.length > 0) {
                const fotoMetadata = await metadataOphalen(n++, url)
                pauzeren(2, 4)
                if (fotoMetadata) fotosMetadata.push(fotoMetadata)
            }
        }
        let datumStempel = Date.now()
        const csv = new ObjectsToCsv(fotosMetadata)
        const csvString = await csv.toString()
        // TODO
        // Betere foutmelding
        return { timestamp: datumStempel, json: fotosMetadata.length > 0 ? fotosMetadata : 'Geen metadata', csv: csvString }
    } catch (error) {
        console.log(error);
    }
}

// linksScrapen()

// exports.scraper = scraper
